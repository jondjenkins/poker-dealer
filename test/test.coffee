should = require 'should'
request = require 'supertest'
app = require('../server.js').app
resources = require('../server.js').resources

game = {}
players = [] # for x-api-key reference only (game will contain most current player defs)

describe 'poker happy path (with a few basic rule validations)', ->
  describe 'new game', ->
    it 'gets valid game with status "not started"', (done) ->
      request(app).get('/game.json')
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          game = res.body
          game.should.be.an.instanceOf Object
          game.should.have.property('status').eql 'not started'
          game.should.have.property('remainingCards').eql 52
          done()

  describe 'create players', ->
    it 'creates player named "bob"', (done) ->
      request(app).post('/game.json/player/bob')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          res.body.should.be.an.instanceOf Object
          res.body.should.have.property('name').eql 'bob'
          players.push res.body
          if err then return done(err)
          done()
    it 'creates player named "martha"', (done) ->
      request(app).post('/game.json/player/martha')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          res.body.should.be.an.instanceOf Object
          res.body.should.have.property('name').eql 'martha'
          players.push res.body
          if err then return done(err)
          done()
    it 'creates player named "joe"', (done) ->
      request(app).post('/game.json/player/joe')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          res.body.should.be.an.instanceOf Object
          res.body.should.have.property('name').eql 'joe'
          players.push res.body
          if err then return done(err)
          done()
    it 'creates player named "kathy"', (done) ->
      request(app).post('/game.json/player/kathy')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          res.body.should.be.an.instanceOf Object
          res.body.should.have.property('name').eql 'kathy'
          players.push res.body
          if err then return done(err)
          done()
    it 'verifies four total players', (done) ->
      request(app).get('/game.json')
      .expect(200)
      .end (err, res) ->
          res.body.players.should.have.lengthOf(4)
          game = res.body
          if err then return done(err)
          done()

  describe 'start game', ->
    it 'changes game status to "in progress" and verifies player in focus', (done) ->
      request(app).put('/game.json/start')
      .set('x-api-key', players[0]['x-api-key'])# first player bob elects to start
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'in progress'
              game.focusPlayer.name.should.eql 'bob'
              game.players[0].name.should.eql 'bob'
              game.players[0].hasFocus.should.eql true
              done()

  describe 'one player folds immediately', ->
    it 'verifies "bob" can fold and "martha" becomes next player', (done) ->
      request(app).put('/game.json/fold')
      .set('x-api-key', players[0]['x-api-key']) # first player bob elects to fold
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'in progress'
              game.players[0].hasFocus.should.eql false
              game.players[0].availableActions.should.eql [] # random validation of correctly set properties...
              game.players[0].folded.should.eql true
              game.focusPlayer.name.should.eql 'martha'
              game.players[1].hasFocus.should.eql true
              done()

  describe 'first betting round', ->
    it 'verifies "martha" can bet 3 chips to open the betting round and "joe" becomes the next player', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[1]['x-api-key'])# second player martha bets 3
      .send({'chips': 3})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.players[1].chips.should.eql 7
              game.players[1].bet.should.eql 3
              game.players[1].hasFocus.should.eql false
              game.players[1].availableActions.should.eql []
              game.pot.should.eql 3
              game.focusPlayer.name.should.eql 'joe'
              game.players[2].hasFocus.should.eql true
              done()
    it 'ensures that "joe" must bet at least 3 chips and "kathy" becomes the next player', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[2]['x-api-key'])
      .send({'chips': 2})
      .expect(400)
      .end (err, res) ->
          res.body.should.eql { code: 400, reason: 'invalid action: insufficient bet (game bet is at 3)' }
          if err then return done(err)
          request(app).put('/game.json/bet')
          .set('x-api-key', players[2]['x-api-key'])
          .send({'chips': 3})
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              request(app).get('/game.json')
              .expect(200)
              .end (err, res) ->
                  if err then return done(err)
                  game = res.body
                  game.players[2].chips.should.eql 7
                  game.players[2].bet.should.eql 3
                  game.players[2].hasFocus.should.eql false
                  game.players[2].availableActions.should.eql []
                  game.pot.should.eql 6
                  game.focusPlayer.name.should.eql 'kathy'
                  game.players[3].hasFocus.should.eql true
                  game.players[3].hand.cards.length.should.eql 3
                  done()
    it 'validates that player "martha" cannot play out of turn', (done) ->
      request(app).put('/game.json/fold')
      .set('x-api-key', players[1]['x-api-key'])
      .expect(400)
      .end (err, res) ->
          if err then return done(err)
          done()
    it 'confirms fourth card is dealt to all remaining players once "kathy" has bet 3 chips', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[3]['x-api-key'])
      .send({'chips': 3})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.players[3].chips.should.eql 7
              game.players[3].hasFocus.should.eql false
              game.players[3].availableActions.should.eql []
              game.pot.should.eql 9
              game.focusPlayer.name.should.eql 'martha' # bob is folded
              game.players[1].hasFocus.should.eql true
              game.players[0].hand.cards.length.should.eql 3 # no more cards for bob, he folded
              game.players[1].hand.cards.length.should.eql 4
              game.players[2].hand.cards.length.should.eql 4
              game.players[3].hand.cards.length.should.eql 4
              game.bet.should.eql 0
              game.players[1].bet.should.eql 0
              game.players[2].bet.should.eql 0
              game.players[3].bet.should.eql 0
              done()

  describe 'second betting round, somebody raises, winner declared', ->
    it 'verifies "martha" can bet 3 chips to open the betting round and "joe" becomes the next player', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[1]['x-api-key'])# second player martha bets 3
      .send({'chips': 2})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.players[1].chips.should.eql 5
              game.players[1].bet.should.eql 2
              game.players[1].hasFocus.should.eql false
              game.players[1].availableActions.should.eql []
              game.pot.should.eql 11
              game.focusPlayer.name.should.eql 'joe'
              game.players[2].hasFocus.should.eql true
              done()
    it 'verifies "joe" can fold and "kathy" becomes next player', (done) ->
      request(app).put('/game.json/fold')
      .set('x-api-key', players[2]['x-api-key'])# first player bob elects to fold
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'in progress'
              game.players[2].hasFocus.should.eql false
              game.players[2].availableActions.should.eql [] # random validation of correctly set properties...
              game.players[2].folded.should.eql true
              game.focusPlayer.name.should.eql 'kathy'
              game.players[3].hasFocus.should.eql true
              done()
    it 'verifies "kathy" can raise by 1 chip (bets 3 chips) and "martha" becomes the next player', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[3]['x-api-key'])
      .send({'chips': 3})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.players[3].chips.should.eql 4
              game.players[3].bet.should.eql 3
              game.players[3].hasFocus.should.eql false
              game.players[3].availableActions.should.eql []
              game.pot.should.eql 14
              game.focusPlayer.name.should.eql 'martha'
              game.players[1].hasFocus.should.eql true
              done()
    it 'verifies "martha" cannot check (bet 0 chips)', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[1]['x-api-key'])
      .send({'chips': 0})
      .expect(400)
      .end (err, res) ->
          if err then return done(err)
          done()
    it 'verifies "martha" can raise by 1 chip (bets 2 chips) and "kathy" becomes the next player', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[1]['x-api-key'])
      .send({'chips': 2})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.players[1].chips.should.eql 3
              game.players[1].bet.should.eql 4
              game.players[1].hasFocus.should.eql false
              game.players[1].availableActions.should.eql []
              game.bet.should.eql 4
              game.pot.should.eql 16
              game.focusPlayer.name.should.eql 'kathy'
              game.players[3].hasFocus.should.eql true
              done()
    it 'verifies "kathy" can call with 1 chip, fifth card is dealt, game is scored, winner declared', (done) ->
      request(app).put('/game.json/bet')
      .set('x-api-key', players[3]['x-api-key'])
      .send({'chips': 1})
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.status.should.eql 'completed'
              game.pot.should.eql 0
              game.bet.should.eql 0
              should.equal(game.focusPlayer, null)
              done()

  describe 'allow new player to join', ->
    it 'creates player named "will"', (done) ->
      request(app).post('/game.json/player/will')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          res.body.should.be.an.instanceOf Object
          res.body.should.have.property('name').eql 'will'
          players.push res.body
          done()

  describe 'allow game to reset & resume with (5) current players', ->
    it 'resets game and verifies player in focus', (done) ->
      request(app).put('/game.json/reset')
      .set('x-api-key', 'codedojo')
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'reset'
              game.players.length.should.eql 5
              done()
    it 'changes game status to "in progress" and verifies player in focus', (done) ->
      request(app).put('/game.json/start')
      .set('x-api-key', players[4]['x-api-key']) # new player will elects to start
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'in progress'
              game.focusPlayer.name.should.eql 'bob'
              game.players[0].name.should.eql 'bob'
              game.players[0].hasFocus.should.eql true
              done()
    it 'completely resets game', (done) ->
      request(app).put('/game.json/reset')
      .set('x-api-key', players[0]['x-api-key'])
      .expect(200)
      .end (err, res) ->
          if err then return done(err)
          request(app).get('/game.json')
          .expect(200)
          .end (err, res) ->
              if err then return done(err)
              game = res.body
              game.should.have.property('status').eql 'not started'
              should.equal(game.focusPlayer, null)
              game.players.length.should.eql 0
              game.pot.should.eql 0
              game.bet.should.eql 0
              console.log game.log
              done()

    # TODO: verify players with no chips were removed, continue game until somebody wins the entire pot, test "all in" situations etc.