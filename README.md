# poker

RESTful poker game and dealer web APIs

## Usage

You can invoke the API from a command line, as follows:

    curl --header "x-api-key: codedojo" -i -X POST http://localhost:8001/game.json/newuser

Swagger documentation is available at [http://localhost:8001/docs](http://localhost:8001/docs), if the Node.js app is running locally on the default local port (8001).