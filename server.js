// uses
var express = require('express'),
    url = require('url'),
    swagger = require('./node_swagger/swagger.js'),
    model_game = require('./include/model_game.js'),
    model_player = require('./include/model_player.js'),
    model_action = require('./include/model_action.js'),
    resources = exports.resources = require('./include/resources.js'); // export for test debugging

// init
var app = exports.app = express(); // export for testing

app.configure(function () {
    app.use(express.bodyParser());
    app.use(function (err, req, res, next) {
        res.send(400, 'Invalid request, see console for details');
        console.error(err.stack);
    });
});
swagger.setAppHandler(app);

swagger.setHeaders = function setHeaders(res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-api-key');
    res.header('Content-Type', 'application/json; charset=utf-8');
};

// validate
swagger.addValidator(
    function validate(req, path, httpMethod) {
        if ('POST' == httpMethod || 'DELETE' == httpMethod || 'PUT' == httpMethod) {
            var apiKey = req.headers['x-api-key'];
            if (!apiKey)
                apiKey = url.parse(req.url, true).query['x-api-key'];
            // allow any valid player api key
            for (var i = 0, players = resources.data.game.players.length; i < players; i++)
                if (resources.data.game.players[i]['x-api-key'] == apiKey) {
                    // piggyback on request to pass along validated player for subsequent use/validation
                    req.validatedPlayer = resources.data.game.players[i];
                    return true;
                }
            // allow default api key (non-PUT only TBD)
            if ('codedojo' == apiKey)
                return true;
            return false;
        }
        return true;
    }
);

// api routes
swagger.addModels(model_game)
    .addModels(model_player)
    .addModels(model_action)
    .addGet(resources.tableStatus)
    .addGet(resources.findPlayer)
    .addPost(resources.joinGame)
    .addPut(resources.updateGame);

swagger.configure(null, '0.1');

// doc route
var docs_handler = express.static(__dirname + '/swagger-ui-1.1.13/');
app.get(/^\/docs(\/.*)?$/, function (req, res, next) {
    if (req.url === '/docs') {
        res.writeHead(302, { 'Location': req.url + '/' });
        res.end();
        return;
    }
    req.url = req.url.substr('/docs'.length);
    return docs_handler(req, res, next);
});

// TODO: consider removing this and just using _Game.timeLastAction on "next" REST call
// reset game every 5 seconds (TODO: expand process to handle lack of game progress)
//function reset() {
//    resources.data.game = new model_game._Game(1);
//}
//setInterval(reset, 5000);

// start
app.listen(process.env.PORT || 8001);