/******************************************************************************
 * Playing Card Objects                                                        *
 *                                                                             *
 * Do not remove this notice.                                                  *
 *                                                                             *
 * Copyright 2001 by Mike Hall                                                 *
 * Please see http://www.brainjar.com for terms of use.                        *
 ******************************************************************************/

//=============================================================================
// Card Object
//
// Note: Requires cards.css for display.
//=============================================================================

//-----------------------------------------------------------------------------
// Card constructor function.
//-----------------------------------------------------------------------------

function Card(rank, suit) {

    this.rank = rank;
    this.suit = suit;

    this.toString = cardToString;
//    this.createNode = cardCreateNode;
}

//-----------------------------------------------------------------------------
// cardToString(): Returns the name of a card (including rank and suit) as a
// text string.
//-----------------------------------------------------------------------------

function cardToString() {

    var rank, suit;

    switch (this.rank) {
        case 'A' :
            rank = 'Ace';
            break;
        case '2' :
            rank = 'Two';
            break;
        case '3' :
            rank = 'Three';
            break;
        case '4' :
            rank = 'Four';
            break;
        case '5' :
            rank = 'Five';
            break;
        case '6' :
            rank = 'Six';
            break;
        case '7' :
            rank = 'Seven';
            break;
        case '8' :
            rank = 'Eight';
            break;
        case '9' :
            rank = 'Nine';
            break;
        case '10' :
            rank = 'Ten';
            break;
        case 'J' :
            rank = 'Jack'
            break;
        case 'Q' :
            rank = 'Queen'
            break;
        case 'K' :
            rank = 'King'
            break;
        default :
            rank = null;
            break;
    }

    switch (this.suit) {
        case 'C' :
            suit = 'Clubs';
            break;
        case 'D' :
            suit = 'Diamonds'
            break;
        case 'H' :
            suit = 'Hearts'
            break;
        case 'S' :
            suit = 'Spades'
            break;
        default :
            suit = null;
            break;
    }

    if (rank == null || suit == null)
        return '';

    return rank + ' of ' + suit;
}


//=============================================================================
// Stack Object
//=============================================================================

//-----------------------------------------------------------------------------
// Stack constructor function.
//-----------------------------------------------------------------------------

function Stack() {

    // Create an empty array of cards.

    this.cards = new Array();

    this.makeDeck = stackMakeDeck;
    this.shuffle = stackShuffle;
    this.deal = stackDeal;
    this.draw = stackDraw;
    this.addCard = stackAddCard;
    this.combine = stackCombine;
    this.cardCount = stackCardCount;
    this.stringify = stackCollapse; // 20131104.jj
    this.score = stackScorePoker; // 20131104.jj
    this.describe = stackDescribePoker; // 20131104.jj
}

function stackCollapse() {
    var cardList = '';
    for (var i = 0; i < this.cards.length; i++)
        cardList = (cardList == '') ? this.cards[i].rank + this.cards[i].suit : cardList + ',' + this.cards[i].rank + this.cards[i].suit;
    return cardList;
}

//-----------------------------------------------------------------------------
// stackMakeDeck(n): Initializes a stack using 'n' packs of cards.
//-----------------------------------------------------------------------------

function stackMakeDeck(n) {

    var ranks = new Array('A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K');
    var suits = new Array('C', 'D', 'H', 'S');
    var i, j, k;
    var m;

    m = ranks.length * suits.length;

    // Set array of cards.

    this.cards = new Array(n * m);

    // Fill the array with 'n' packs of cards.

    for (i = 0; i < n; i++)
        for (j = 0; j < suits.length; j++)
            for (k = 0; k < ranks.length; k++)
                this.cards[i * m + j * ranks.length + k] = new Card(ranks[k], suits[j]);
}

//-----------------------------------------------------------------------------
// stackShuffle(n): Shuffles a stack of cards 'n' times. 
//-----------------------------------------------------------------------------

function stackShuffle(n) {

    var i, j, k;
    var temp;

    // Shuffle the stack 'n' times.

    for (i = 0; i < n; i++)
        for (j = 0; j < this.cards.length; j++) {
            k = Math.floor(Math.random() * this.cards.length);
            temp = this.cards[j];
            this.cards[j] = this.cards[k];
            this.cards[k] = temp;
        }
}

//-----------------------------------------------------------------------------
// stackDeal(): Removes the first card in the stack and returns it.
//-----------------------------------------------------------------------------

function stackDeal() {

    if (this.cards.length > 0)
        return this.cards.shift();
    else
        return null;
}

//-----------------------------------------------------------------------------
// stackDraw(n): Removes the indicated card from the stack and returns it.
//-----------------------------------------------------------------------------

function stackDraw(n) {

    var card;

    if (n >= 0 && n < this.cards.length) {
        card = this.cards[n];
        this.cards.splice(n, 1);
    }
    else
        card = null;

    return card;
}

//-----------------------------------------------------------------------------
// stackAdd(card): Adds the given card to the stack.
//-----------------------------------------------------------------------------

function stackAddCard(card) {

    this.cards.push(card);
}

//-----------------------------------------------------------------------------
// stackCombine(stack): Adds the cards in the given stack to the current one.
// The given stack is emptied.
//-----------------------------------------------------------------------------

function stackCombine(stack) {

    this.cards = this.cards.concat(stack.cards);
    stack.cards = new Array();
}

//-----------------------------------------------------------------------------
// stackCardCount(): Returns the number of cards currently in the stack.
//-----------------------------------------------------------------------------

function stackCardCount() {

    return this.cards.length;
}

// *****************************************************************
// https://github.com/iguigova/snippets_js/tree/master/pokerIn4Hours
// *****************************************************************

function stackScorePoker(name) {

    // 20131105.jj hacks to comply with original method parameters
    var input = (name == undefined) ? 'unknown,' + this.stringify() : name + ',' + this.stringify(); // 20131104.jj keep name which was required in old implementation

    input = input.replace(/\s+/g, '').replace(/,[Jj]/g, ',11').replace(/,[Qq]/g, ',12').replace(/,[Kk]/g, ',13').replace(/,[Aa]/g, ',14').toUpperCase().split(',');

    var hand = {D: [], H: [], C: [], S: []};
    for (var i = 1, len = input.length; i < len; i++) {
        input[i] && (hand[input[i].slice(input[i].length - 1)][input[i].slice(0, input[i].length - 1)] = 1);
    }

    var card = function (suite, rank) {
        return hand[suite][rank] || 0
    };
    var cards = function (rank) {
        return card('D', rank) + card('H', rank) + card('C', rank) + card('S', rank);
    };
    var kickers = function (idx) { // http://en.wikipedia.org/wiki/Kicker_(poker)
        idx = idx || -15;
        var notplayed = Math.max(input.length - 1/*player input*/ - 5, 0);
        return function (all, cardinality, rank) {
            return (all || 0) + (((cardinality == 1) && (notplayed-- <= 0)) ? rank * Math.pow(10, ++idx) : 0);
        };
    }();

    var tag = function (a, b, always) {
        a = a || 0;
        b = Math.min(b || 0, 1);
        return (b || always) ? a + b : 0
    };
    var reset = function (a) {
        return (a < 5) ? 0 : a
    };

    var cardsofrank = [];
    var hc = 0;         // high card
    var k4 = 0;         // four of a kind
    var k3 = 0;         // three of a kind
    var p2 = 0;         // two pair / two one pairs
    var p1 = 0;         // one pair / two of a kind
    var k = 0;          // kickers
    var sd = cards(14); // straight discriminant: count A as 1 or 14
    for (var i = 2; i < 15; i++) {
        cardsofrank[i] = cards(i);
        hc = (cardsofrank[i]) ? i * Math.pow(10, -4) : hc;
        k4 = (cardsofrank[i] === 4) ? hc : k4;
        k3 = (cardsofrank[i] === 3) ? hc : k3;
        p2 = (cardsofrank[i] === 2) ? p1 : p2;
        p1 = (cardsofrank[i] === 2) ? hc : p1;
        k = kickers(k, cardsofrank[i], i);
        sd = tag(sd, cardsofrank[i], sd >= 5);
    }
    ;
    var s = reset(sd); // straight

    if (s && cards(14) && !cards(13)) {
        k = k - 14 * Math.pow(10, sd);
    } // adjust for A as 1 or 14

    var cardsofsuite = {D: 0, H: 0, C: 0, S: 0};
    for (var i = 2; i < 15; i++) {
        cardsofsuite['D'] = tag(cardsofsuite['D'], card('D', i), true);
        cardsofsuite['H'] = tag(cardsofsuite['H'], card('H', i), true);
        cardsofsuite['C'] = tag(cardsofsuite['C'], card('C', i), true);
        cardsofsuite['S'] = tag(cardsofsuite['S'], card('S', i), true);
    }
    var f = reset(cardsofsuite['D']) + reset(cardsofsuite['H']) + reset(cardsofsuite['C']) + reset(cardsofsuite['S']);  // flush

    var score = function (cond, bigendian, littleendian) {
        return (cond ? 1 : 0) * (bigendian + littleendian);
    };

    return {
        player: input[0],
        points: (score(s && f, 8, k)                              // straightflush
            || score(k4, 7, k4)                              // fourofakind
            || score(p1 && k3, 6, p1 + k3 * Math.pow(10, 2)) // fullhouse
            || score(f, 5, k)                                // flush
            || score(s, 4, k)                                // straight
            || score(k3, 3, k3)                              // threeofakind
            || score(p2, 2, p2 + p1 * Math.pow(10, 2))       // twopair
            || score(p1, 1, p1))                             // onepair
            + score(hc, 0, k)                                    // highcard - tie breaker
    };
};

// ******************************************************************
// http://www.codeproject.com/Articles/186361/Poker-In-Four-Hours#CD
// calculates the rank of a 5 card poker hand using bit manipulations
// ******************************************************************

function stackDescribePoker() {

    var hands = ['4 of a Kind', 'Straight Flush', 'Straight', 'Flush', 'High Card',
        '1 Pair', '2 Pair', 'Royal Flush', '3 of a Kind', 'Full House' ];
    var _ = { 'C': 1, 'S': 2, 'H': 4, 'D': 8 };

    // 20131105.jj hacks to comply with original method parameters
    var cards = this.stringify().split(',');
    var cs = [],
        ss = [];
    for (var i = 0; i < cards.length; i++) {
        cs.push(cards[i][0]);
        ss.push(_[cards[i][1]]);
    }

    for (var i = 0; i < cs.length; i++)
    switch (cs[i]) {
        case 'J': cs[i] = 11; break;
        case 'Q': cs[i] = 12; break;
        case 'K': cs[i] = 13; break;
        case 'A': cs[i] = 14; break;
    }

    var v, i, o, s = 1 << cs[0] | 1 << cs[1] | 1 << cs[2] | 1 << cs[3] | 1 << cs[4];
    for (i = -1, v = o = 0; i < 5; i++, o = Math.pow(2, cs[i] * 4)) {
        v += o * ((v / o & 15) + 1);
    }
    v = v % 15 - ((s / (s & -s) == 31) || (s == 0x403c) ? 3 : 1);
    v -= (ss[0] == (ss[1] | ss[2] | ss[3] | ss[4])) * ((s == 0x7c00) ? -5 : 1);

    return hands[v] + ((s == 0x403c) ? ' (Ace low)' : '');
}

//////////////////////////////////////////

module.exports = {
    Card: Card,
    Stack: Stack
}