var sw = require('../node_swagger/swagger.js'),
    swe = sw.errors,
    param = require('../node_swagger/paramTypes.js'),
    url = require('url'),
    data = require('./data.js');

exports.data = data; // pass up to server for managing game TODO: remove this if unused

function renderJson(res, body) {

    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Content-Length', body.length);
    res.send(body);
}

exports.joinGame = {
    'spec': {
        'description': 'joinGame description',
        'path': '/game.{format}/player/{name}',
        'notes': 'Allows new player to join game if there is room at single deck table. Note that all subsequent API calls on behalf of a player using PUT method will require use of personalized API key delivered as x-api-key header. Be sure to save x-api-key after this request as it will only be returned from server once.',
        'summary': 'Create new player for game',
        'method': 'POST',
        'params': [param.path('name', 'Player name, no spaces, alphanumeric', 'string')],
        'responseClass': 'Player',
        'errorResponses': [swe.invalid('name'), swe.invalid('game')],
        'nickname': 'joinGame'
    },
    'action': function (req, res) {

        if (data.game.status == 'in progress')
            throw swe.invalid('game: in progress');
        else if (data.game.players.length > 9) // 10 players max in 52 card deck
            throw swe.invalid('game: no seats remaining');

        var player = data.game.addPlayer(req.params.name);
        if (player)
            renderJson(res, player);
        else
            throw swe.invalid('name: already taken or invalid');
    }
};

exports.findPlayer = {
    'spec': {
        'description': 'findPlayer description',
        'path': '/game.{format}/player/{name}',
        'notes': 'Allows search for public view of player, by name.',
        'summary': 'Look up player by name',
        'method': 'GET',
        'params': [param.path('name', 'Player name, no spaces', 'string')],
        'responseClass': 'PlayerPublic',
        'errorResponses': [swe.invalid('name'), swe.invalid('request')],
        'nickname': 'findPlayer'
    },
    'action': function (req, res) {

        var player = data.game.findPlayer(req.params.name, true); // public player view, hide x-api-key
        if (player)
            renderJson(res, player);
        else
            throw swe.invalid('name');
    }
}

exports.updateGame = {
    'spec': {
        'description': 'updateGame description',
        'path': '/game.{format}/{action}',
        'notes': 'Allows player to take an action against current game. Actions available to player for game play are referenced in the Player(Public) object. Requires use of personalized x-api-key header. The player-specific API key was delivered to player when player was created.',
        'summary': 'Player-initiated game action',
        'method': 'PUT',
        'params': [param.path('action', 'Valid action keyword (start, fold, bet, calculate, reset)', 'string'),
            param.body('Chips', 'Number of chips for action (optional)', 'long')],
        'errorResponses': [swe.invalid('action')],
        'nickname': 'updateGame'
    },
    'action': function (req, res) {

        var ac = req.params.action;
        if (ac == 'fold' || ac == 'bet') // add check, call, raise vs. just bet in future?
            ac = 'player';

        switch (ac) {

            case 'start':

                var err = data.game.doAction('start');
                if (err)
                    throw swe.invalid('action: ' + err);
                else
                    res.end();
                break;

            case 'player':

                if (req.validatedPlayer == undefined)
                    throw swe.invalid('action: player not specified via valid x-api-key');

                if (data.game.status != 'in progress')
                    throw swe.invalid('action: game not in progress');

                if (req.validatedPlayer.hasFocus) {

                    if (req.validatedPlayer.availableActions.indexOf(req.params.action) == -1)
                        throw swe.invalid('action: action not available now');

                    switch (req.params.action) {

                        case 'fold':

                            var err = data.game.doAction('fold', req.validatedPlayer);
                            if (!err)
                                res.end();
                            else
                                throw swe.invalid('action: could not fold');
                            break;

                        case 'bet':

                            if (isNaN(req.body.chips))
                                throw swe.invalid('action: must bet positive integer');

                            if (req.validatedPlayer.chips < req.body.chips)
                                throw swe.invalid('action: not enough chips');

                            if ((req.body.chips + req.validatedPlayer.bet) < data.game.bet && req.body.chips != req.validatedPlayer.chips) // allow "all in" when chips less than current bet
                                throw swe.invalid('action: insufficient bet (game bet is at ' + data.game.bet + ')');

                            var err = data.game.doAction('bet', req.validatedPlayer, req.body.chips);
                            if (!err)
                                res.end();
                            else
                                throw swe.invalid('action: could not bet');
                            break;

                        default:

                            throw swe.invalid('action: invalid action request');
                            break;
                    }
                }
                else
                    throw swe.invalid('action: player does not have focus');
                break;

            case 'calculate':

                var winner = data.game.doAction('calculate');
                if (!winner)
                    throw swe.invalid('action: game still in progress');
                else
                    res.send(winner);
                break;

            case 'reset':

                if (req.validatedPlayer == undefined && data.game.status == 'in progress')
                    throw swe.invalid('action: only a player can reset the game while it is in progress');
                else if (req.validatedPlayer !== undefined && data.game.status == 'in progress')
                    data.game.reset(1, true); // hard reset
                else
                    data.game.reset(1, false);
                res.end();
                break;

            default:

                throw swe.invalid('action');
                break;
        }
    }
}

exports.tableStatus = {
    'spec': {
        'description': 'tableStatus description',
        'path': '/game.{format}',
        'notes': 'Returns public view of game table and related player status. Will not return private data such as player-specific x-api-key.',
        'summary': 'Get current game table status',
        'method': 'GET',
        'responseClass': 'GamePublic',
        'nickname': 'tableStatus'
    },
    'action': function (req, res) {

        renderJson(res, data.game.tableStatus());
    }
}