var cards = require('./cards.js'),
    common = require('./common.js');

// global init
Object.prototype.clone = common.clone;

function validateName(name) {

    var username = /^[a-z0-9]+$/i;
    if (name.match(username))
        return true;
    else
        return false;
}

function _Player(name) {

    this.name = name;
    this.folded = false;
    this.hasFocus = false;
    this.availableActions = [];
    this['x-api-key'] = common.uuid();
    this.hand = new cards.Stack();
    this.score = 0;
    this.handDesc = null;
    this.chips = 10; // default for new player
    this.bet = 0; // bet for current hand
    this.folded = false;
    this.isWinner = false;
}

_Player.prototype.refresh = function () {

    this.score = (this.hand.cardCount() > 0) ? this.hand.score().points : 0;
    this.handDesc = (this.hand.cardCount() > 0) ? this.hand.describe() : null;
}

_Player.prototype.public = function (keepApiKey) {

    // default parameters
    keepApiKey = (keepApiKey !== undefined) ? keepApiKey : false;

    var player = this.clone();
    if (!keepApiKey) delete player['x-api-key'];
    delete player.score;
    return player;
}

module.exports = {
    _Player: _Player,
    validateName: validateName,
    "PlayerPublic": {
        "id": "PlayerPublic",
        "properties": {
            "name": {
                "type": "string"
            },
            "folded": {
                "type": "boolean"
            },
            "hasFocus": {
                "type": "boolean"
            },
            "availableActions": {
                "allowableValues": {
                    "valueType": "LIST",
                    "values": [
                        "fold",
                        "check",
                        "bet",
                        "raise",
                        "call"
                    ],
                    "valueType": "LIST"
                },
                "description": "actions available to player while in focus",
                "type": "Array"
            },
            "hand": {
                type: "Object"
            },
            "handDesc": {
                "type": "string"
            },
            "chips": {
                "type": "long"
            },
            "bet": {
                "type": "long"
            },
            "isWinner": {
                "type": "boolean"
            }
        }
    },
    "Player": {
        "id": "Player",
        "properties": {
            "name": {
                "type": "string"
            },
            "folded": {
                "type": "boolean"
            },
            "hasFocus": {
                "type": "boolean"
            },
            "availableActions": {
                "allowableValues": {
                    "valueType": "LIST",
                    "values": [
                        "fold",
                        "check",
                        "bet",
                        "raise",
                        "call"
                    ],
                    "valueType": "LIST"
                },
                "description": "actions available to player while in focus",
                "type": "Array"
            },
            "x-api-key": {
                "type": "string"
            },
            "hand": {
                type: "Object"
            },
            "score": {
                "type": "long"
            },
            "handDesc": {
                "type": "string"
            },
            "chips": {
                "type": "long"
            },
            "bet": {
                "type": "long"
            },
            "isWinner": {
                "type": "boolean"
            }
        }
    }
}
