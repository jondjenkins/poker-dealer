var model_player = require('./model_player.js'),
    cards = require('./cards.js'),
    common = require('./common.js');

// global init
Object.prototype.clone = common.clone;

function _Game(decks) {

    // initialize cards.js objects
    var deck;
    deck = new cards.Stack();
    deck.makeDeck(decks);
    deck.shuffle(decks);

    // construct game
    this.status = 'not started';
    this.timeLastAction = null; // TODO: if no valid, next action for > 5 minutes, reset entire game
    this.players = [];
    this.focusPlayer = null;
    this.pot = 0;
    this.bet = 0;
    this.log = []; // messages showing recent history
    this.remainingCards = deck.cardCount();
    this.deck = deck;
}

// allow reset of game and ability to define who opens play
_Game.prototype.reset = function (decks, removePlayers, firstPlayer) {

    // default parameters
    removePlayers = (removePlayers !== undefined) ? removePlayers : false;
    firstPlayer = (firstPlayer !== undefined) ? firstPlayer : undefined;

    // initialize cards.js objects
    var deck;
    deck = new cards.Stack();
    deck.makeDeck(decks);
    deck.shuffle(decks);

    // reset game
    this.status = 'reset';
    this.timeLastAction = new Date();
    this.focusPlayer = null;
    this.pot = 0;
    this.bet = 0;
    this.remainingCards = deck.cardCount();
    this.deck = deck;

    if (!removePlayers) {
        // remove any player with no chips
        for (var i = 0, players = this.players.length; i < players; i++)
            if (this.players[i].chips == 0)
                this.players[i].pop();
            else {
                this.players[i].folded = false;
                this.players[i].hand = new cards.Stack();
                this.deal(this.players[i], 3);
            }

        // allow player order to update if name is provided
        if (firstPlayer !== undefined) {
            var beforePlayer = [];
            var afterPlayer = [];
            var player = null;
            var pos = -1;
            for (var i = 0, players = this.players.length; i < players; i++) {
                if ((this.players[i].name == firstPlayer) && (pos = -1)) { // matched player, first time
                    pos = i;
                    player = this.players[i];
                } else if ((this.players[i].name != firstPlayer) && (pos = -1)) { // did not match, haven't found yet
                    beforePlayer.push(this.players[i]);
                } else if ((this.players[i].name != firstPlayer) && (pos != -1)) { // did not match, found
                    afterPlayer.push(this.players[i]);
                }
            }
            this.players = [];
            this.players.push(player);
            this.players.concat(afterPlayer);
            this.players.concat(beforePlayer);
        }
        this.doLog('Game reset, players retained (those with no chips removed)');
    }
    else {
        this.status = 'not started'
        this.players = [];
        this.doLog('Game forcibly reset to original status, all players removed');
    }
}

_Game.prototype.doLog = function (entry) {

    this.log.push({entry: entry, timestamp: new Date().toISOString()});
    while (this.log.length > 100) // cap log size
        this.log.pop();
}

_Game.prototype.getNextPlayer = function (currentPlayer) {
    var name = currentPlayer.name;
    var pos = -1;
    for (var i = 0, players = this.players.length; i < players; i++)
        if (this.players[i].name == name)
            pos = i;
    if (pos < this.players.length - 1)
        for (var i = pos + 1, players = this.players.length; i < players; i++)
            if (!this.players[i].folded)
                return this.players[i];
    for (var i = 0; i < pos; i++)
        if (!this.players[i].folded)
            return this.players[i];
    return null;
}

_Game.prototype.debugPlayers = function () { // TODO: this is mainly used during testing ... refactor into test suite?
    console.log('* ... folded');
    for (var i = 0, players = this.players.length; i < players; i++) {
        var aa = (this.players[i].availableActions.length != 0) ? this.players[i].availableActions : 'N/A';
        var f = (this.players[i].folded) ? '* ' : '';
        console.log(f + this.players[i].name + ' | ' + this.players[i].hand.stringify() + ' | (bet) ' + this.players[i].bet + ' | (chips) ' + this.players[i].chips + ' | (hasFocus) ' + this.players[i].hasFocus + ' | (actions) ' + aa);
    }
    console.log('---');
    console.log(this.log);
}

_Game.prototype.doAction = function (action, player, data) {

    this.timeLastAction = new Date().toISOString();

    switch (action) {

        case 'start':

            if (this.status != 'in progress') { // TODO: may wish to move these checks to resources.js for consistency
                if (this.players.length > 1) {
                    this.status = 'in progress';
                    // establish betting focus, opening actions
                    this.focusPlayer = this.players[0];
                    this.players[0].hasFocus = true;
                    this.players[0].availableActions = ['fold', 'bet']; // may wish to elaborate into raise, call, check etc. in future
                    this.doLog('Game started via service request');
                    return false;
                }
                else
                    return 'insufficient players (' + this.players.length + ')';
            }
            else
                return 'game in progress';
            break;

        case 'fold':

            player.folded = true;
            player.availableActions = [];
            player.hasFocus = false;
            this.doLog('Player "' + player.name + '" folded with hand "' + player.hand.stringify() + '"');

            var nextPlayer = this.getNextPlayer(player);
            var validActions = ['fold', 'bet']; // TODO: make this work, DRY (see below)

            nextPlayer.hasFocus = true;
            this.focusPlayer = nextPlayer;
            nextPlayer.availableActions = validActions;

            break;

        case 'bet':

            player.chips -= data;
            player.bet += data; // reflect last bet at player level
            this.bet = player.bet; // reflect last bet at game level (use player bet since it has been summed)
            this.pot += data;
            player.availableActions = [];
            player.hasFocus = false;

            if (player.chips == 0)
                this.doLog('Player "' + player.name + '" went all in (' + data + ' chips) having hand "' + player.hand.stringify() + '"');
            else
                this.doLog('Player "' + player.name + '" bet ' + data + ' chips having hand "' + player.hand.stringify() + '"');

            var nextPlayer = this.getNextPlayer(player);
            var validActions = ['fold', 'bet'];

            nextPlayer.hasFocus = true;
            this.focusPlayer = nextPlayer;

            nextPlayer.availableActions = validActions;

            // deal next card if applicable and clear bets
            if (nextPlayer.hand.cardCount() < 5) {
                var evenBet = true;
                for (var i = 0, players = this.players.length; i < players; i++)
                    if (this.players[i].bet < this.bet && !this.players[i].folded)
                        evenBet = false;
                if (evenBet) {
                    for (var i = 0, players = this.players.length; i < players; i++)
                        if (!this.players[i].folded) {
                            this.players[i].bet = 0;
                            this.deal(this.players[i], 1);
                        }
                    this.bet = 0;
                    this.doLog('Dealt next card to all remaining players (' + nextPlayer.hand.cardCount() + 'th card)');
                    if (nextPlayer.hand.cardCount() == 5) { // declare a winner
                        nextPlayer.hasFocus = false;
                        nextPlayer.availableActions = [];
                        this.focusPlayer = null;
                        this.doAction('calculate');
                    }
                }
            }

            break;

        case 'calculate':

            // only allow when game is fully dealt
            for (var i = 0; i < this.players.length; i++)
                if (this.players[i].hand.cardCount() < 5 && !this.players[i].folded)
                    return false;

            var winner = new model_player._Player(''); // placeholder for comparator score = 0
            for (var i = 0, players = this.players.length; i < players; i++) {
                if (!this.players[i].folded) {
                    this.players[i].refresh();
                    this.doLog('Player "' + this.players[i].name + '" completed game with cards (' + this.players[i].hand.stringify() + ') forming hand (' + this.players[i].handDesc + ') having score (' + this.players[i].score + ')')
                    if (this.players[i].score > winner.score)
                        winner = this.players[i];
                }
            }
            winner.isWinner = true;
            winner.chips += this.pot;
            this.pot = 0;
            this.status = 'completed';
            this.doLog('Player "' + winner.name + '" determined as winner with high score (' + winner.score + ') and hand (' + winner.handDesc + ')');
            return winner;
            break;

        default:

            return undefined;
            break;
    }
}

_Game.prototype.deal = function (player, numCards) {
    if (this.deck.cardCount() < numCards)
        return false;
    for (var i = 0; i < numCards; i++)
        player.hand.addCard(this.deck.deal());
    this.remainingCards = this.deck.cardCount();
    player.refresh();
    return true;
}

_Game.prototype.addPlayer = function (name) {

    // validation
    if (!model_player.validateName(name))
        return false;

    var player = (this.findPlayer(name) == undefined) ? new model_player._Player(name) : false;

    if (player) {
        if (this.status == 'not started')
            this.deal(player, 3);
        this.players.push(player);
        this.doLog('Player "' + player.name + '" joined game');
        this.timeLastAction = new Date();
        return player.public(true);
    }
    else
        return false;
};

_Game.prototype.findPlayer = function (name, renderPublic) {

    // default parameters
    renderPublic = (renderPublic !== undefined) ? renderPublic : false;

    for (var i = 0; i < this.players.length; i++) {
        if (this.players[i].name == name) {
            this.players[i].refresh();
            if (renderPublic)
                return this.players[i].public();
            else
                return this.players[i];
        }
    }
    return undefined;
}

_Game.prototype.tableStatus = function () {

    // make view model public-safe
    var game = this.clone();
    for (var i = 0; i < game.players.length; i++)
        game.players[i] = game.players[i].public();
    game.focusPlayer = (game.focusPlayer) ? game.focusPlayer.public() : null;
    delete game.deck;
    return game;
}

module.exports = {
    _Game: _Game,
    "GamePublic": { // deck not visible
        "id": "GamePublic",
        "properties": {
            "status": {
                "allowableValues": {
                    "valueType": "LIST",
                    "values": [
                        "not started",
                        "in progress",
                        "completed",
                        "reset"
                    ],
                    "valueType": "LIST"
                },
                "description": "game status",
                "type": "string"
            },
            "timeLastAction": {
                "type": "Date"
            },
            "players": {
                "items": {
                    "$ref": "PlayerPublic"
                },
                "type": "Array"
            },
            "focusPlayer": {
                "type": "PlayerPublic"
            },
            "pot": {
                "type": "long"
            },
            "bet": {
                "type": "long"
            },
            "log": {
                "items": {
                    "type": "Object"
                },
                "type": "Array"
            },
            "remainingCards": {
                "type": "long"
            }
        }
    },
    "Game": {
        "id": "Game",
        "properties": {
            "status": {
                "allowableValues": {
                    "valueType": "LIST",
                    "values": [
                        "not started",
                        "in progress",
                        "completed",
                        "reset"
                    ],
                    "valueType": "LIST"
                },
                "description": "game status",
                "type": "string"
            },
            "timeLastAction": {
                "type": "Date"
            },
            "players": {
                "items": {
                    "$ref": "Player"
                },
                "type": "Array"
            },
            "focusPlayer": {
                "type": "Player"
            },
            "pot": {
                "type": "long"
            },
            "bet": {
                "type": "long"
            },
            "log": {
                "items": {
                    "type": "Object"
                },
                "type": "Array"
            },
            "remainingCards": {
                "type": "long"
            },
            "deck": {
                "type": "Object"
            }
        }
    }
}